'use strict';

// https://www.ssense.com/en-us/men/product/burberry/green-tailoring-blazer/3743379
const regex = /ticketing\.fondazioneprada\.org/gi;

class Prada {
    constructor() {
        this.regex = regex;
    }

    validUrl(url) {
        return this.regex.test(url);
    }

    async getData(page) {
        return page.evaluate(() => {
            const header = document.querySelector('.page-header');
            const title = header.querySelector('h1').textContent.trim();
            const town = header.querySelector('.location').textContent.trim();
            const date = document.getElementById('dataora').textContent.trim();
            const imageWrapper = document.getElementById('divLocandina');
            const relUrl = imageWrapper.querySelector('img').getAttribute('src');
            const pos = window.location.pathname.lastIndexOf('/');

            const path = window.location.pathname.substr(0, pos) + '/' + relUrl;
            const image = (new URL(path, window.location).toString());

            function textNodesUnder(node) {
                let all = [];
                for (node = node.firstChild; node; node = node.nextSibling) {
                    if (node.nodeType === 3) all.push(node);
                    else all = all.concat(textNodesUnder(node));
                }
                return all;
            }

            const fullDescription = document.getElementById('divtrama');
            let director = '';
            let actors = [];

            const textNodes = textNodesUnder(fullDescription);
            for (let textNode of textNodes) {
                const text = textNode.textContent;
                if (text.search(/regia/ig) !== -1) {
                    const parts = text.split(':');
                    director = parts[1].trim();
                } else if (text.search(/Attori/ig) !== -1) {
                    const parts = text.split(':');
                    const actorsStr = parts[1].trim();
                    actors = actorsStr.split(',').map(el => el.trim());
                }
            }

            return {
                title: title,
                town: town,
                date: date,
                image: image,
                director: director,
                actors: actors
            };
        });
    }

    async scrape(options, browser) {
        const url = options.url;
        const page = await browser.newPage();
        await page.setViewport({width: 1920, height: 1040});
        await page.goto(url, {waitUntil: 'networkidle0'});
        const result = await this.getData(page);
        await page.close();
        return result;
    }
}

module.exports = Prada;