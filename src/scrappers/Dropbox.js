'use strict';

// https://www.dropbox.com/s/779gu53f0eu3iuf/UBS%20Newsletters.zip?dl=0
const regex = /dropbox\.com/gi;
const fs = require('fs');
const path = require('path');
const tmpDirectory = './tmp';

const rimraf = require("rimraf");
const AdmZip = require('adm-zip');
const emlformat = require('eml-format');

class Dropbox {
    constructor() {
        this.regex = regex;
    }

    validUrl(url) {
        return this.regex.test(url);
    }

    static async createTmpDirectory() {
        return new Promise((resolve, reject) => {
            fs.mkdir(tmpDirectory, { recursive: true }, (err) => {
                if (err) {
                    reject(err);
                } else {
                    resolve();
                }
            });
        });
    }

    static async deleteTmpDirectory() {
        return new Promise((resolve, reject) => {
            rimraf(tmpDirectory, err => {
                if (err) {
                    reject(err);
                } else {
                    resolve();
                }
            });
        })
    }

    static checkDownloading() {
        return new Promise((resolve, reject) => {
            fs.readdir(tmpDirectory, function(err, items) {
                if (err) {
                    reject(err);
                } else {
                    resolve(items)
                }
            });
        })
    }

    static async getFileResponse(page) {
        await page._client.send('Page.setDownloadBehavior', {
            behavior: 'allow',
            downloadPath: tmpDirectory,
        });
        await page.click('.mc-popover-trigger', { delay: 300 });
        await page.click('.mc-popover-content-item', { delay: 300 });

        let notDownloaded = true;
        while (notDownloaded) {
            await page.waitFor(1000);
            const files = await Dropbox.checkDownloading();
            for (let file of files) {
                if (file.indexOf('.crdownload') === -1) {
                    notDownloaded = false;
                }
            }
        }
    }

    static async saveFile(path, buffer) {
        return new Promise((resolve, reject) => {
            fs.writeFile(path, buffer, err => {
                if (err) {
                    reject(err);
                } else {
                    resolve();
                }
            })
        })
    }

    static async openFile(path) {
        return new Promise((resolve, reject) => {
            fs.readFile(path, (err, data) => {
                if (err) {
                    reject(err);
                } else {
                    resolve(data);
                }
            })
        })
    }

    static async unzipFile(filesToFind) {
        const files = await Dropbox.checkDownloading();
        for (let file of files) {
            const fullPath = path.join(tmpDirectory, file);
            const zip = new AdmZip(fullPath);
            const zipEntries = zip.getEntries();
            for (let entry of zipEntries) {
                if (entry.isDirectory) continue;
                const name = entry.name;
                if (filesToFind.includes(name)) {
                    const buffer = entry.getData();
                    const entryPath = path.join(tmpDirectory, name);
                    await Dropbox.saveFile(entryPath, buffer);
                }
            }
        }
    }

    static async readEml(eml) {
        return new Promise((resolve, reject) => {
            emlformat.read(eml, (err, data) => {
               if (err) {
                   reject(err);
               } else {
                   resolve(data);
               }
            });
        });
    }

    static async processUnzipped(filesToProcess, page) {
        let result = [];
        for (let file of filesToProcess) {
            const fullPath = path.join(tmpDirectory, file);
            try {
                const eml = await Dropbox.openFile(fullPath);
                const rawResult = await Dropbox.readEml(eml.toString("utf-8"));
                const html = rawResult.html;
                await page.setContent(html);
                result = result.concat(await Dropbox.parseEmlHtmlContent(page));
            } catch (e) {
                console.error(e);
            }
        }
        return result;
    }

    static async parseEmlHtmlContent(page) {
        return [];
        /*
        return page.evaluate(() => {
            const result = [];

            const trs = document.getElementsByTagName('tr');
            let trsClean = [];
            const cleanTr = (tr) => {
                const table = tr.querySelector('table');
                if (table) {
                    let trsInnner = table.getElementsByTagName('tr');
                    for (let trInner of trsInnner) {
                        cleanTr(tr);
                    }
                } else {
                    trsClean.push(tr);
                }
            };
            for (let tr of trs) {
                cleanTr(tr);
            }
            trsClean = [...new Set(trsClean)];

            let obj = {};
            for (let tr of trsClean) {
                const img = tr.querySelector('img');
                if (img) {
                    if (obj.image) {
                        result.push(obj);
                    }
                    obj.image = img.getAttribute('src');
                    obj.title = img.getAttribute('alt');
                }
                const text = tr.textContent.trim();
                if (text.length === 0) continue;

            }

            return result;
        });
        */
    }

    async scrape(options, browser) {
        const url = options.url;
        const page = await browser.newPage();
        await page.setViewport({width: 1920, height: 1040});
        await Dropbox.deleteTmpDirectory();
        await Dropbox.createTmpDirectory();
        await page.goto(url);
        await page.waitFor(1000);
        await Dropbox.getFileResponse(page);
        await Dropbox.unzipFile(options.files);
        const result = await Dropbox.processUnzipped(options.files, page);
        await Dropbox.deleteTmpDirectory();
        await page.close();
        return result;
    }
}

module.exports = Dropbox;