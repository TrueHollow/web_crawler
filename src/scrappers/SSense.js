'use strict';

// https://www.ssense.com/en-us/men/product/burberry/green-tailoring-blazer/3743379
const regex = /ssense\.com/gi;

async function autoScroll(page){
    await page.evaluate(async () => {
        await new Promise((resolve) => {
            let totalHeight = 0;
            const distance = 100;
            const timer = setInterval(() => {
                const scrollHeight = document.body.scrollHeight;
                window.scrollBy(0, distance);
                totalHeight += distance;

                if(totalHeight >= scrollHeight){
                    clearInterval(timer);
                    resolve();
                }
            }, 100);
        });
    });
}

class SSense {
    constructor() {
        this.regex = regex;
    }

    validUrl(url) {
        return this.regex.test(url);
    }

    async getData(page) {
        return page.evaluate(() => {
            const title = document.querySelector('.product-brand').textContent.trim();
            const subtitle = document.querySelector('.product-name').textContent.trim();
            const fullDescription = document.querySelector('.product-description-text');
            const description = fullDescription.querySelector('span').textContent.trim();
            const price = document.querySelector('.price').textContent.trim();

            const selectSizes = document.getElementById('size');
            const options = selectSizes.options;
            const sizes = [];
            const re = /Sold Out/ig;
            for (let i = 1, length = options.length; i < length; ++i) {
                const option = options[i].textContent;
                const parts = option.split('=');
                if (parts.length === 2) {
                    sizes.push(parts[1].trim());
                } else {
                    if (!re.test(option)) {
                        sizes.push(option.trim());
                    }
                }
            }
            const images = [];
            const imageWrappers = document.querySelectorAll('.image-wrapper');
            for (let i = 0, length = imageWrappers.length; i < length; ++i) {
                const wrapper = imageWrappers[i];
                const srcLink = wrapper.querySelector('source').getAttribute('srcset');
                images.push(srcLink);
            }

            return {
                title: title,
                subtitle: subtitle,
                description: description,
                price: price,
                sizes: sizes,
                images: images
            };
        });
    }

    async scrape(options, browser) {
        const url = options.url;
        const page = await browser.newPage();
        await page.setViewport({width: 1920, height: 1040});
        await page.goto(url, {waitUntil: 'networkidle0'});
        await autoScroll(page);
        const result = await this.getData(page);
        await page.close();
        return result;
    }
}

module.exports = SSense;