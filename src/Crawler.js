'use strict';

const puppeteer = require("puppeteer-extra");

const pluginStealth = require("puppeteer-extra-plugin-stealth");
puppeteer.use(pluginStealth());

const SSense = require('./scrappers/SSense');
const Prada = require('./scrappers/Prada');
const Dropbox = require('./scrappers/Dropbox');

class Crawler {
    constructor(configuration) {
        this.configuration = configuration;
        this.browser = null;
        this.scrappers = [
            new SSense(),
            new Prada(),
            new Dropbox()
        ];
    }

    async init() {
        await this.close();
        this.browser = await puppeteer.launch(this.configuration);
    }

    async scrape(options) {
        if (!options || !options.url) return;
        const url = options.url;
        const scrapper = this.scrappers.find(scrapper => {
            return scrapper.validUrl(url);
        });
        if (scrapper) {
            return await scrapper.scrape(options, this.browser);
        } else {
            throw new TypeError('Scrapper not found.');
        }
    }

    async close() {
        if (this.browser) {
            await this.browser.close();
        }
    }
}

module.exports = Crawler;