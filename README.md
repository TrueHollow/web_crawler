## Preparation

### Node version

Tested on

```sh
node --version
v11.13.0
```


### Create configuration file for your enviroment


```json
{
  "chrome": {
    "args": [
      "--no-sandbox",
      "--disable-setuid-sandbox",
      "--disable-infobars",
      "--window-position=0,0",
      "--ignore-certifcate-errors",
      "--ignore-certifcate-errors-spki-list"
    ],
    "headless": false,
    "ignoreHTTPSErrors": true
  }
}
```

### Install dependencies

Run command ```npm install```

### Running tests

Execute command ```npm test```