'use strict';

const config = require('./config/default');
const Crawler = require('./src/Crawler');

const singletonCrawler = new Crawler(config.chrome);

module.exports = singletonCrawler;