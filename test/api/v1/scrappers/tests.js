'use strict';

const expect = require('chai').expect;
const singletonCrawler = require('../../../../index');

describe('Scrappers', () => {
    before(async () => {
        await singletonCrawler.init();
    });

    it('test unknown', async () => {
        const url = `https://www.example.com`;
        const options = {
            url: url
        };
        let result = await singletonCrawler.scrape();
        expect(result).to.be.an('undefined');
        result = await singletonCrawler.scrape({});
        expect(result).to.be.an('undefined');
    });

    it('test SSense', async () => {
        const url = `https://www.ssense.com/en-us/women/product/collina-strada/blue-garden-mechanic-coat/3785239`;
        const options = {
            url: url
        };
        const result = await singletonCrawler.scrape(options);

        const example = {
            "title": "Burberry",
            "subtitle": "Green Tailoring Blazer",
            "description": "Long sleeve wool blazer in green. Notched lapel collar. Double-breasted press-stud closure and darts at front. Welt pocket at chest. Flap pockets at waist. Four-button surgeon's cuffs. Padded shoulders. Vented back hem. Logo patch and welt pockets at interior. Fully lined. Silver-tone hardware. Supplier color: Matcha green",
            "price": "$1650 USD",
            "sizes": [
                "XXS",
                "XS",
                "S",
                "M",
                "L"
            ],
            "images": [
                "https://img.ssensemedia.com/image/upload/b_white/c_scale,h_820/f_auto,dpr_1.0/191376M195003_1.jpg",
                "https://img.ssensemedia.com/image/upload/b_white/c_scale,h_820/f_auto,dpr_1.0/191376M195003_2.jpg",
                "https://img.ssensemedia.com/image/upload/b_white/c_scale,h_820/f_auto,dpr_1.0/191376M195003_3.jpg",
                "https://img.ssensemedia.com/image/upload/b_white/c_scale,h_820/f_auto,dpr_1.0/191376M195003_4.jpg",
                null
            ]
        };

        expect(result).to.be.an('object');
        console.log(result);
        //expect(result).to.eql(example);
    });

    it('test Prada', async () => {
        const url = `http://ticketing.fondazioneprada.org/ticketing/UIPublic/CinemaDettaglio.aspx?idEvento=7581`;
        const options = {
            url: url
        };
        const result = await singletonCrawler.scrape(options);

        const example = {
            "title": "THE KILLERS (1964)",
            "town": "Milano",
            "date": "07 aprile 2019 19:00",
            "image": "http://ticketing.fondazioneprada.org/ticketing/UIPublic/locandine/THEKILLERS1964.JPG",
            "director": "Don Siegel",
            "actors": "Lee Marvin, Angie Dickinson, John Cassavetes"
        };

        expect(result).to.be.an('object');
        console.log(result);
        //expect(result).to.eql(example);
    });

    it('test Dropbox', async () => {
        const url = `https://www.dropbox.com/s/779gu53f0eu3iuf/UBS%20Newsletters.zip?dl=0`;
        const options = {
            url: url,
            files: ['Mail Attachment-1.eml']
        };
        const result = await singletonCrawler.scrape(options);

        expect(result).to.be.an('array');
    });

    after(async () => {
        await singletonCrawler.close();
    });
});